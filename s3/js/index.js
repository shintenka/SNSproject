var vm = new Vue({
  el: "#app", // Vue.jsを使うタグのIDを指定
  data: {
    // Vue.jsで使う変数はここに記述する
    post:{
      //投稿用
      userId: localStorage.getItem('userId'),
      postForm:{
        text:null,
        category:null
      },
      //検索用
      posts:[],
      query:{
        userId: null,
        category: null,
        start: null,
        end: null
      }
  }
},

  computed: {
    // 計算した結果を変数として利用したいときはここに記述する
    sortedPosts: function(){
      'use strict';
      return
      this.posts.sort(function(a, b){
        return b.timestamp - a.timestamp;
      });
    }
  },
  created: function() {
    // Vue.jsの読み込みが完了したときに実行する処理はここに記述する
    'use strict';
    fetch(url + "/posts",{
      method:'GET',
      headers:new headers({
        "Authorization":
        localStorage.getItem('token')
      })
    })
    .then(function(response){
      if(response.ok){
        return response.json();
      }
      return
      response.json().then(function(json){
        throw new
        Error(json.message);
      });
    })
    .then(function(json){
      vm.posts.forEach(function(e){
        e.data = new
        Data(e.timestamp).toLocaleString();
      });
    })
    .catch(function(err){
      window.console.error(err.message);
    });
  },

   methods: {
    // Vue.jsで使う関数はここで記述する
post: function() {
//send
fetch(url + "/post", {
    method: "POST",
    headers:new headers({
        "Authorization":
        localStorage.getItem('token')
      }),
    body: JSON.stringify({
      "userId":vm.userId,
      "text":vm.postForm.text,
      "category":vm.postForm.category
    })
  })
  .then(function(response) {
    if (response.ok) {
      return response.json();
    }
    // 200番以外のレスポンスはエラーを投げる
    return response.json().then(function(json) {
      throw new Error(json.message);
    });
  })
  .then(function(json) {
    // レスポンスが200番で返ってきたときの処理はここに記述する
     json.date = new Date(json.timestamp).toLocaleString();
          vm.posts.unshift(json);
          vm.postForm.text = null;
          vm.postForm.category = null;
   //console.log(JSON.stringify(json));
  })
  .catch(function(err) {
    // レスポンスがエラーで返ってきたときの処理はここに記述する
    window.console.error(err.message);
  });
  },
  //search: function() {
}
});
