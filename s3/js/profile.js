var vm = new Vue({
  el: "#app", // Vue.jsを使うタグのIDを指定
  data: {
    // Vue.jsで使う変数はここに記述する
    user:{
      userId: null,
      password: null,
      nickname: null,
      age: null
    }
  },
  methods: {
    // Vue.jsで使う関数はここで記述する
submit: function() {
// APIにPOSTリクエストを送る
    fetch(url + "/user", {
        method: "PUT",
        headers:new Headers({ "Authorization":localStorage.getItem('token')}),
        body: JSON.stringify({
          "userId":localStorage.getItem('userId'),
          "password":vm.user.password,
          "nickname":vm.user.nickname,
          "age":Number(vm.user.age)

        })

    }).then(function(response) {
      if (response.status == 200) {
        return response.json();
      }
      // 200番以外のレスポンスはエラーを投げる
      return response.json().then(function(json) {
        throw new Error(json.message);
      });
    })
    .then(function(json) {
      // レスポンスが200番で返ってきたときの処理はここに記述する
      var content = JSON.stringify(json, null, 2);
      console.log(content);
     //console.log(JSON.stringify(json));
    })
    .catch(function(err) {
      // レスポンスがエラーで返ってきたときの処理はここに記述する
    });
},

    deleteUser:function(){
       
      //----------削除----------
        fetch(url + "/user", {
          method: "DELETE",
          headers:new Headers({ "Authorization":localStorage.getItem('token')}),
          body: JSON.stringify({
          "userId":localStorage.getItem('userId')
        })
        }).then(function(response) {
          if (response.status == 200) {
            return response.json();
          }
          // 200番以外のレスポンスはエラーを投げる
          return response.json().then(function (json) {
                throw new Error(json.message);
          });
        })
        .then(function (json) {
            // レスポンスが200番で返ってきたときの処理はここに記述する 
            localStorage.setItem('token', ""); 
            location.href = "./login.html";
         })
        .catch(function (err) {
      // レスポンスがエラーで返ってきたときの処理はここに記述する 
        });
      //-----------------------------------------------------
    }
},
  created: function() {
              // Vue.jsの読み込みが完了したときに実行する処理はここに記述する
              // APIにGETリクエストを送る
          fetch(url + "/user" +
               "?userId="+localStorage.userId, {
                 method: "GET"
              })
             .then(function(response) {
               if (response.status == 200) {
                return response.json();
               }
               // 200番以外のレスポンスはエラーを投げる
               return response.json().then(function(json) {
                 throw new Error(json.message);
               });
             })
             .then(function(json) {
             // レスポンスが200番で返ってきたときの処理はここに記述する
             console.log(json)
               vm.user.userId = json.Item.userId
             })
            .catch(function(err) {
               // レスポンスがエラーで返ってきたときの処理はここに記述する
             });

},
  computed: {
    // 計算した結果を変数として利用したいときはここに記述する
  }
});

new Vue({
  el: "#app"
});
