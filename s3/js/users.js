var vm = new Vue({
  el: "#app", // Vue.jsを使うタグのIDを指定
  data: {
    // Vue.jsで使う変数はここに記述する
    users:[],
    query:{
      nickname: null,
      start: null,
      end: null
    }
  },
  methods: {
    // Vue.jsで使う関数はここで記述する
  },
  created: function() {
    // Vue.jsの読み込みが完了したときに実行する処理はここに記述する
    fetch(url + "/users", {
          method: "GET"
 //         headers:new Headers({ "Authorization":localStorage.getItem('token')}),
 //         body: JSON.stringify({
 //     "userId":vm.user.userId,
  //    "nickname":vm.user.nickname,
  //    "age":Number(vm.user.age)
  //  })
        }).then(function(response) {
          if (response.status == 200) {
            return response.json();
          }
          // 200番以外のレスポンスはエラーを投げる
          return response.json().then(function (json) {
                throw new Error(json.message);
          });
        })
        .then(function (json) {
          console.log(json)
            // レスポンスが200番で返ってきたときの処理はここに記述する 
            //for (var i = json.Item.length - 1; i >= 0; i--) {
              
              vm.users = json.users
          //  }
         })
        .catch(function (err) {
      // レスポンスがエラーで返ってきたときの処理はここに記述する 
        });
      //-----------------------------------------------------
    },
computed:{
    // 計算した結果を変数として利用したいときはここに記述する
filteredUsers: function() {
    var result = this.users;

      if (this.query.nickname) {
        result = result.filter(function (target) {
          if(target.nickname) return target.nickname.match(vm.query.nickname);
       }); 
      }
      if (this.query.start) {
        result = result.filter(function (target) {
          return target.age >= vm.query.start; 
        });
      }
      if (this.query.end) {
       result = result.filter(function (target) { 
        return target.age <= vm.query.end;
       }); 
      }
      return result; 
    }
  }
});
