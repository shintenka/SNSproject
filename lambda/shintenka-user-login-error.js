var AWS = require("aws-sdk");
var dynamo = new AWS.DynamoDB.DocumentClient();
var tableName = "user";

exports.handler = (event, context, callback) =>{
//レスポンスの雛形
var response = {
statusCode: 200,
headers: {
	"Access-Control-Allow-Origin" : "*"
},
body: JSON.stringify({"message" : ""})
};

//リクエストbodyを取得し、jsのオブジェクトにパースする 
var body = JSON.parse(event.body);
//bodyが空だったら返す 
if(!body){
response.statusCode = 400;
response.body = JSON.stringify({"message" : "bodyが空です"}); 
callback(null, response);
return;
} 
 //bodyの中身を取得
var userId = body.userId;
var age = body.age;
var password = body.password; 
var nickname = body.nickname;

//userテーブルのvalidation(パラメータのどれかが空だったら返す) 
if(!userId || !password || !nickname || !age){
response.statusCode = 400;
response.body = JSON.stringify({"message" : "パラメータが足りません"}); 
callback(null, response);
return;
}
//登録するデータ内容 
var item = {
"userId" : userId,
"age" : age, 
"password" : password, 
"nickname" : nickname
};
var param = {
"TableName" : tableName,
"Item" : item
};
//データの登録
dynamo.put(param, function(err, data){
//登録に失敗した場合 
if(err){
response.statusCode = 400;
response.body = JSON.stringify({"message" : "データの登録に失 敗しました"});
callback(null, response);
return; //登録に成功した場合 
}else{
delete param.Item.password;//パスワードは返さない
response.body = JSON.stringify(param.Item); 
callback(null, response);
return;
} 
});
};


