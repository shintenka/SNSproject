var AWS = require("aws-sdk");
var dynamo = new AWS.DynamoDB.DocumentClient();
var tableName = "post";

exports.handler = (event, context, callback) => {
    var response = {
        statusCode: 200,
        headers: {
            "Access-Control-Allow-Origin" : "*"
        },
        body: JSON.stringify({"message" : ""})
    };

//url param

    var userId = event.queryStringParameters.userId; 
    var start = event.queryStringParameters.start;
    var end = event.queryStringParameters.end;
    var category = event.queryStringParameters.category;

    //TODO: query()に渡すparamを宣言
    if(userId && start && end &&category){

        var param = {
            "TableName" :tableName ,
            //キー、インデックスによる検索の定義
            "KeyConditionExpression" :
                "userId= :uid AND #timestamp BETWEEN :start AND :end ",
            //プライマリーキー以外の属性でのフィルタ
            "FilterExpression":
                "#category = :category",
                
            //属性名のプレースホルダの定義
            "ExpressionAttributeNames" : {
                "#category":"category",
                "#timestamp":"timestamp"
            },
            //検索値のプレースホルダの定義
            "ExpressionAttributeValues" : {
                ":uid": userId,
                ":start": Number(start),
                ":end":Number(end),
                ":category":category
            } 
        }
    }else if(userId && start && end){
        var param = {
            "TableName" :tableName ,
            //キー、インデックスによる検索の定義
            "KeyConditionExpression" :
                "userId= :uid AND #timestamp BETWEEN :start AND :end ",
            //プライマリーキー以外の属性でのフィルタ
            // "FilterExpression":
            //     "#timestamp = :timestamp",
                
            //属性名のプレースホルダの定義
            "ExpressionAttributeNames" : {
                "#timestamp":"timestamp"
            },
            //検索値のプレースホルダの定義
            "ExpressionAttributeValues" : {
                ":uid": userId,
                ":start": Number(start),
                ":end":Number(end)
            }
        }

    }else{
        var param = {
            "TableName" :tableName ,
            //キー、インデックスによる検索の定義
            "KeyConditionExpression" :
                "userId= :uid",

            //検索値のプレースホルダの定義
            "ExpressionAttributeValues" : {
                ":uid": userId
            }
        }

    }

    //dynamo.query()を用いてuserIdとpasswordが一致するデータの検索
    dynamo.query(param, function(err, data){
        //userの取得に失敗
        if(err){
            response.statusCode = 500;
            response.body = JSON.stringify({"message" : err});
            callback(null, response);
            return;
        }
        //TODO: 該当するデータが見つからない場合の処理を記述(ヒント：data.Itemsの中身が空)
        if(!data.Items.length){
            console.log(JSON.stringify(data))
            response.statusCode = 401;
            response.body = JSON.stringify({
                "message": "検索結果なし"
            })
            callback(null, response);
            return;
        }
        //TODO: 認証が成功した場合のレスポンスボディとコールバックを記述
        response.body = JSON.stringify(data.Items);
        callback(null, response);

    });
};
